package seleniumhm.pageobject.gmail;

import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;
import seleniumhm.driver.DriverClient;
import seleniumhm.element.casual.Button;
import seleniumhm.element.casual.TextInput;
import seleniumhm.pageobject.AbstractPage;
import seleniumhm.utils.ConfigReaders;
import seleniumhm.utils.CustomWaiters;

public class GmailHomePage extends AbstractPage {
    private static Logger LOG = LogManager.getLogger(GmailHomePage.class);

    public GmailHomePage() {
    }

    @FindBy(xpath = "//div[@jscontroller]//div[@role='button']")
    private Button composeButton;

    @FindBy(xpath = "//table[@role='group']//div[@role='button' and text()]")
    private Button sendButton;

    @FindBy(xpath = "(//input[@type='text' and @aria-label])[1]")
    private TextInput searchField;

    @FindBy(xpath = "//*[contains(@href,'sent')]//parent::span//parent::div/..")
    private Button sentMailButton;

    @FindBy(xpath = "//*[contains(@href,'drafts')]//parent::span//parent::div/..")
    private Button draftsButton;

    @FindBy(xpath = "//textarea[@role='combobox']")
    private TextInput destinationEmailField;

    @FindBy(name = "subjectbox")
    private TextInput subjectBoxField;

    @FindBy(xpath = "//div[@role='textbox']")
    private TextInput messageTextField;

    @FindBy(xpath = "(//tr//parent::tbody//tr[@role='row'])[1]")
    private Button firstDraftMessage;

    @FindBy(xpath = "//*[@id='link_vsm']")
    private Button popupViewMessageButton;

    @Step("Click compose button step")
    public GmailHomePage clickComposeButton() {
        CustomWaiters.waitForElementToBeClickable(composeButton).click();
        LOG.info("compose button was clicked");
        return this;
    }

    @Step("Click send message button step")
    public GmailHomePage clickSendMessageButton() {
        CustomWaiters.waitForElementToBeClickable(sendButton).click();
        LOG.info("send button was clicked");
        return this;
    }

    @Step("Click sent mail button step step")
    public GmailHomePage clickSentMailButton() {
        CustomWaiters.waitForVisibilityOfElement(sentMailButton).click();
        LOG.info("sentMail button was clicked");
        return this;
    }

    @Step("Click drafts button step")
    public GmailHomePage clickDraftsButton() {
        CustomWaiters.waitForVisibilityOfElement(draftsButton).click();
        LOG.info("drafts button was clicked");
        return this;
    }

    @Step("Submit search field step")
    public GmailHomePage submitSearchField() {
        CustomWaiters.waitForVisibilityOfElement(searchField).submit();
        LOG.info(" search field was submitted");
        return this;
    }

    @Step("Click popup view message step")
    public GmailHomePage clickPopUpViewMessageButton() {
        CustomWaiters.waitForVisibilityOfElement(popupViewMessageButton).click();
        LOG.info(" view message button was clicked");
        return this;
    }

    @Step("Click first draft message step")
    public GmailHomePage clickFirstDraftMessage() {
        CustomWaiters.waitForElementToBeClickable(firstDraftMessage).click();
        LOG.info("first draft message was clicked");
        return this;
    }

    @Step("Enter search field: {0} step")
    public GmailHomePage enterSeachField(String text) {
        CustomWaiters.waitForVisibilityOfElement(searchField).sendKeys(text);
        LOG.info("text " + text + " was entered into search field");
        return this;
    }

    @Step("Enter destination email: {0} step")
    public GmailHomePage enterDestinationEmail(String destinationEmail) {
        CustomWaiters.waitForVisibilityOfElement(destinationEmailField).sendKeys(destinationEmail + "\n");
        LOG.info(" email " + destinationEmail + " was entered");
        return this;
    }

    @Step("Enter subject: {0} step")
    public GmailHomePage enterSubject(String subject) {
        CustomWaiters.waitForVisibilityOfElement(subjectBoxField).sendKeys(subject);
        LOG.info("subject " + subject + "was entered");
        return this;
    }

    @Step("Enter message text: {0} step")
    public GmailHomePage enterMessageText(String message) {
        CustomWaiters.waitForVisibilityOfElement(messageTextField).sendKeys(message);
        LOG.info("message was entered");
        return this;
    }

    @Step("Wait for url contains text step")
    public GmailHomePage waitForUrlContainsText(String text) {
        CustomWaiters.waitForUrlContainsText(text);
        return this;
    }

    @Step("Proceed to sent messages step")
    public GmailHomePage proceedToSentMessages() {
        DriverClient.getDriver().get(ConfigReaders.read("base_sent"));
        return this;
    }

    @Step("Proceed to drafts step")
    public GmailHomePage proceedToDrafts() {
        DriverClient.getDriver().get(ConfigReaders.read("base_drafts"));
        return this;
    }
}
