package seleniumhm.driver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.util.Optional;

public class DriverClient {
    private static Logger LOG = LogManager.getLogger(DriverClient.class);
    private static ThreadLocal<WebDriver> DRIVER_POOL = new ThreadLocal<>();

    public static WebDriver getDriver() {
        return Optional.ofNullable(DRIVER_POOL.get()).isPresent() ?
                /*if present*/      DRIVER_POOL.get() :
                /*if NOT present*/  initializeDriver();
    }

    private static WebDriver initializeDriver() {
        DRIVER_POOL.set(WebDriverProviders.createDriver());
        return DRIVER_POOL.get();
    }

    public static void quitDriver() {
        Optional.ofNullable(DRIVER_POOL).ifPresent(thenExecute -> {
            DRIVER_POOL.get().quit();
            DRIVER_POOL.set(null);
            LOG.debug("WebDriver was closed!");
        });
    }
}
