package seleniumhm;

import org.testng.annotations.DataProvider;
import seleniumhm.model.GoogleAccount;
import seleniumhm.utils.csv.CSVDataProvider;

import java.util.Iterator;

public class AccountDataProvider {
    @DataProvider(parallel = true)
    private Iterator<Object> userList() {
        return CSVDataProvider.getModelsFromFile(GoogleAccount.class, "src/test/resources/AccountData.csv");
    }
}
