package seleniumhm.utils;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import seleniumhm.driver.DriverClient;

import java.util.List;

public abstract class CustomWaiters {
    private static int BASIC_TIMEOUT = Integer.parseInt(ConfigReaders.read("basic_explicit_timeout"));

    public static WebElement waitForVisibilityOfElement(WebElement element) {
        return (new WebDriverWait(DriverClient.getDriver(), BASIC_TIMEOUT))
                .until(ExpectedConditions.visibilityOf(element));
    }

    public static WebElement waitForElementToBeClickable(WebElement element) {
        return (new WebDriverWait(DriverClient.getDriver(), BASIC_TIMEOUT))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public static List<WebElement> waitForVisibilityOfAllElements(List<WebElement> elements) {
        return (new WebDriverWait(DriverClient.getDriver(), BASIC_TIMEOUT))
                .until(ExpectedConditions.visibilityOfAllElements(elements));
    }

    public static Boolean waitForUrlContainsText(String text) {
        return new WebDriverWait(DriverClient.getDriver(), BASIC_TIMEOUT)
                .until((ExpectedConditions.urlContains(text)));
    }
}
