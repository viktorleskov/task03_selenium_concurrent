package seleniumhm.utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import seleniumhm.driver.DriverClient;

public abstract class JSExecutors {
    public static void clickElementThroughJS(WebElement target) {
        JavascriptExecutor executor = (JavascriptExecutor) DriverClient.getDriver();
        executor.executeScript("arguments[0].click();", target);
    }
}
