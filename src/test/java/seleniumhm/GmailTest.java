package seleniumhm;

import io.qameta.allure.Description;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import seleniumhm.driver.DriverClient;
import seleniumhm.model.GoogleAccount;
import seleniumhm.pageobject.gmail.businessobject.GmailHomePageBO;
import seleniumhm.pageobject.gmail.businessobject.GmailLoginPageBO;
import seleniumhm.utils.ConfigReaders;
import seleniumhm.utils.CustomWaiters;
import util.TestListener;

@Listeners({TestListener.class})
public class GmailTest {
    private static org.apache.logging.log4j.Logger LOG;
    private static WebDriver driver;

    @BeforeClass
    static void initAll() {
        LOG = LogManager.getLogger(GmailTest.class);
    }

    @BeforeMethod
    void init() {
        LOG.info("BeforeEach running.");
        driver = DriverClient.getDriver();
        driver.get(ConfigReaders.read("base_page_gmail"));
    }

    @Ignore
    @Test(dataProvider = "userList", dataProviderClass = AccountDataProvider.class)
    @Description("Login test with appropriate credentials")
    void gmailSignInTest(GoogleAccount currentTestAccount) {
        GmailLoginPageBO gmailLoginPageBO = new GmailLoginPageBO();
        gmailLoginPageBO
                .login(currentTestAccount.getEmail(), currentTestAccount.getPassword());
    }

    @Ignore
    @Test(dataProvider = "userList", dataProviderClass = AccountDataProvider.class)
    @Description("Send message test")
    void gmailSendMessageTest(GoogleAccount currentTestAccount) {
        GmailLoginPageBO gmailLoginPageBO = new GmailLoginPageBO();
        GmailHomePageBO gmailHomePageBO = new GmailHomePageBO();
        gmailLoginPageBO
                .login(currentTestAccount.getEmail(), currentTestAccount.getPassword());
        gmailHomePageBO
                .waitForUrlContainsText("inbox")
                .prepareMessage("viktorius490@gmail.com", "hi from selenium", "kuku")
                .clickSendMessageButton()
                .clickPopUpViewMessageButton();
        Assert.assertTrue(CustomWaiters.waitForUrlContainsText("sent"), "message was not sented");
    }

    @Test(dataProvider = "userList", dataProviderClass = AccountDataProvider.class)
    @Description("Send message test after saving as draft")
    void gmailSaveAsDraftThenSendTest(GoogleAccount currentTestAccount) {
        GmailLoginPageBO gmailLoginPageBO = new GmailLoginPageBO();
        GmailHomePageBO gmailHomePageBO = new GmailHomePageBO();
        gmailLoginPageBO
                .login(currentTestAccount.getEmail(), currentTestAccount.getPassword());
        gmailHomePageBO
                .waitForUrlContainsText("inbox")
                .prepareMessage("viktorius490@gmail.com", "hi from selenium", "from draft")
                .clickDraftsButton()
                .clickFirstDraftMessage()
                .clickSendMessageButton()
                .clickPopUpViewMessageButton();
        Assert.assertTrue(CustomWaiters.waitForUrlContainsText("sent"), "message was not sent");
    }

    @AfterMethod
    void tearDown() {
        LOG.info("After each running.");
        DriverClient.quitDriver();
    }

    @AfterClass
    static void tearDownAll() {
        LOG.info("AfterAll test running.");
        LOG = null;
    }

}
