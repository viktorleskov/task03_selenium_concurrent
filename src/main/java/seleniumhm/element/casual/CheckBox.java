package seleniumhm.element.casual;

import org.openqa.selenium.WebElement;
import seleniumhm.element.PageElement;

public class CheckBox extends PageElement {
    public CheckBox(WebElement webElement) {
        super(webElement);
    }

    public void setChecked(boolean value) {
        if (value != isChecked()) {
            webElement.click();
        }
    }

    public boolean isChecked() {
        return webElement.isSelected();
    }
}
