package seleniumhm.utils.csv;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class CSVDataProvider {
    public static Iterator<Object> getModelsFromFile(Class modelToBeProvided, String fileName) {
        List<String> stringDataSet = CSVFileReader.readCSVDataSetByTokens(fileName);
        List<Object> objectsFromFile = new ArrayList<>();
        Field[] fields = modelToBeProvided.getDeclaredFields();
        setAccessForAllFields(fields);
        int fieldCount = fields.length;
        Object newInstance = null;
        for (int currentDataPointer = 0; currentDataPointer < stringDataSet.size(); ) {
            try {
                newInstance = modelToBeProvided.newInstance();
                for (int currentFieldPointer = 0; currentFieldPointer < fieldCount; currentFieldPointer++) {
                    fields[currentFieldPointer].set(newInstance, stringDataSet.get(currentDataPointer));
                    currentDataPointer++;
                }
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            objectsFromFile.add(newInstance);
        }
        return objectsFromFile.iterator();
    }

    private static void setAccessForAllFields(Field[] fields) {
        for (int k = 0; k < fields.length; k++) {
            fields[k].setAccessible(true);
        }
    }
}
