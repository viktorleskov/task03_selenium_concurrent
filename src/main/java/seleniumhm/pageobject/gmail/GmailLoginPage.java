package seleniumhm.pageobject.gmail;

import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;
import seleniumhm.element.casual.Button;
import seleniumhm.element.casual.TextInput;
import seleniumhm.pageobject.AbstractPage;
import seleniumhm.utils.CustomWaiters;

public class GmailLoginPage extends AbstractPage {
    private static Logger LOG = LogManager.getLogger(GmailLoginPage.class);

    public GmailLoginPage() {
    }

    @FindBy(xpath = "//input[@type='email']")
    private TextInput emailField;

    @FindBy(xpath = "//input[@type='password']")
    private TextInput passwordField;

    @FindBy(id = "identifierNext")
    private Button emailNextButton;

    @FindBy(id = "passwordNext")
    private Button passwordNextButton;

    @Step("Enter email: {0} step")
    public GmailLoginPage inputTextInEmailField(String text) {
        CustomWaiters.waitForVisibilityOfElement(emailField).sendKeys(text);
        LOG.info("text :" + text + " was entered");
        return this;
    }

    @Step("Enter password: {0} step")
    public GmailLoginPage inputTextInPasswordField(String text) {
        CustomWaiters.waitForVisibilityOfElement(passwordField).sendKeys(text);
        LOG.info("text :" + text + " was entered");
        return this;
    }

    @Step("Confirm email(click next button) step")
    public GmailLoginPage clickEmailNextButton() {
        CustomWaiters.waitForVisibilityOfElement(emailNextButton).click();
        LOG.info("email field was submitted");
        return this;
    }

    @Step("Confirm password(click next button) step")
    public GmailLoginPage clickPasswordNextButton() {
        CustomWaiters.waitForVisibilityOfElement(passwordNextButton).click();
        LOG.info("password field was submitted");
        return this;
    }

}
