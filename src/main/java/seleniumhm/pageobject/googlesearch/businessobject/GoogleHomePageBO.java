package seleniumhm.pageobject.googlesearch.businessobject;

import seleniumhm.pageobject.googlesearch.GoogleHomePage;

public class GoogleHomePageBO {
    private GoogleHomePage homePage;

    public GoogleHomePageBO() {
        homePage = new GoogleHomePage();
    }

    public GoogleHomePageBO findByText(String text) {
        homePage
                .inputTextInSearchField(text)
                .submitSearchField();
        return this;
    }

    public GoogleHomePageBO clickImageButton() {
        homePage.clickImageButton();
        return this;
    }

}
