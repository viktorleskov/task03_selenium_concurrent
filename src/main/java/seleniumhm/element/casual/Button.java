package seleniumhm.element.casual;

import org.openqa.selenium.WebElement;
import seleniumhm.element.PageElement;

public class Button extends PageElement {
    public Button(WebElement webElement) {
        super(webElement);
    }

    @Override
    public void click() {
        webElement.click();
    }
}
