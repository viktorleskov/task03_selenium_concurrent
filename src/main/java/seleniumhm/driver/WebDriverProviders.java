package seleniumhm.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import seleniumhm.utils.ConfigReaders;

import java.util.concurrent.TimeUnit;

public abstract class WebDriverProviders {
    private static Driver DefaultDriver;
    private static boolean useProfile;

    static {
        DefaultDriver = Driver.valueOf(ConfigReaders.read("default_driver").trim());
        useProfile = Boolean.parseBoolean(ConfigReaders.read("use_profile"));
        System.setProperty("webdriver." + DefaultDriver.name().toLowerCase() + ".driver",
                ConfigReaders.read(DefaultDriver.name().toLowerCase() + "_driver"));
    }

    public static WebDriver createDriver() {
        WebDriver driver = initializeDriver();
        manageGeneralOptions(driver);
        return driver;
    }

    private static WebDriver initializeDriver() {
        if (DefaultDriver == Driver.GECKO) {
            return new FirefoxDriver();
        } else {
            return useProfile ? new ChromeDriver(prepareChromeOptions()) : new ChromeDriver();
        }
    }

    private static ChromeOptions prepareChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--user-data-dir=" + ConfigReaders.read("user_data_dir"),
                "--profile-directory=" + ConfigReaders.read("profile_directory"), "chrome.switches",
                "--disable-extensions");
        return options;
    }

    private static void manageGeneralOptions(WebDriver driver){
        driver.manage().timeouts().
                implicitlyWait(Integer.parseInt(ConfigReaders.read("basic_implicit_timeout")), TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

}
