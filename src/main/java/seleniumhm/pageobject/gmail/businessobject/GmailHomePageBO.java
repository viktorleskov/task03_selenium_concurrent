package seleniumhm.pageobject.gmail.businessobject;

import seleniumhm.pageobject.gmail.GmailHomePage;

public class GmailHomePageBO {
    private GmailHomePage gmailHomePage;

    public GmailHomePageBO() {
        gmailHomePage = new GmailHomePage();
    }

    public GmailHomePageBO prepareMessage(String email, String subject, String message) {
        gmailHomePage
                .clickComposeButton()
                .enterDestinationEmail(email)
                .enterSubject(subject)
                .enterMessageText(message);
        return this;
    }

    public GmailHomePageBO enterTextIntoSearchField(String text) {
        gmailHomePage
                .enterSeachField(text)
                .submitSearchField();
        return this;
    }

    public GmailHomePageBO clickSentMailButton() {
        gmailHomePage
                .clickSentMailButton();
        return this;
    }

    public GmailHomePageBO clickDraftsButton() {
        gmailHomePage
                .clickDraftsButton();
        return this;
    }

    public GmailHomePageBO clickSendMessageButton() {
        gmailHomePage
                .clickSendMessageButton();
        return this;
    }

    public GmailHomePageBO clickPopUpViewMessageButton() {
        gmailHomePage
                .clickPopUpViewMessageButton();
        return this;
    }

    public GmailHomePageBO clickFirstDraftMessage() {
        gmailHomePage
                .clickFirstDraftMessage();
        return this;
    }

    public GmailHomePageBO waitForUrlContainsText(String text) {
        gmailHomePage
                .waitForUrlContainsText(text);
        return this;
    }

    public GmailHomePageBO proceedToSentMessages() {
        gmailHomePage.proceedToSentMessages();
        return this;
    }

    public GmailHomePageBO proceedToDrafts() {
        gmailHomePage.proceedToDrafts();
        return this;
    }

}
