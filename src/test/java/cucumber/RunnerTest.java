package cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import seleniumhm.driver.DriverClient;
import seleniumhm.utils.ConfigReaders;

@CucumberOptions(features = "src/test/resources/gmailcucumber.feature",
        glue = "cucumber.definitions")
public class RunnerTest extends AbstractTestNGCucumberTests {
    private static WebDriver driver;

    @BeforeMethod
    public void init() {
        driver = DriverClient.getDriver();
        driver.get(ConfigReaders.read("base_page_gmail"));
    }

    @AfterMethod
    void tearDown() {
        DriverClient.quitDriver();
    }
}
