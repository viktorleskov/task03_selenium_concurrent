package seleniumhm.pageobject;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import seleniumhm.driver.DriverClient;
import seleniumhm.element.CustomDecorator;

public abstract class AbstractPage {
    public AbstractPage() {
        PageFactory.initElements(new CustomDecorator(new DefaultElementLocatorFactory(DriverClient.getDriver())), this);
    }
}
