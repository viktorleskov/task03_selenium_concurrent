Feature: sending letters

  Scenario Outline: Save message as draft then send
    Given i login with email'<email>', password'<password>' successfully
    Then wait for page load
    And write new message
    And click drafts button
    And click at first draft message
    And click send message button
    And click 'view message' button at popup
    Then sent messages should opens

    Examples:
      | email                  | password        |
      | viktorius490@gmail.com | supervl30111999 |
      | viktorius491@gmail.com | supervl30111999 |
      | viktorius489@gmail.com | supervl30111999 |
      | viktorius493@gmail.com | supervl30111999 |



