package seleniumhm.model;

public class GoogleAccount {
    private String email;
    private String password;

    public GoogleAccount(){}

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "GoogleAccount{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
