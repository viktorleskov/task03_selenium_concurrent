package seleniumhm;

import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import seleniumhm.driver.DriverClient;
import seleniumhm.pageobject.googlesearch.businessobject.GoogleHomePageBO;
import seleniumhm.utils.ConfigReaders;

public class GoogleTest {
    private static org.apache.logging.log4j.Logger LOG;
    private static WebDriver driver;

    @BeforeClass
    static void initAll() {
        LOG = LogManager.getLogger(GoogleTest.class);
    }

    @BeforeMethod
    void init() {
        LOG.info("BeforeEach running.");
        driver = DriverClient.getDriver();
        driver.get(ConfigReaders.read("base_page_google"));
    }

    @Test
    @Ignore
    void googleFindAppleImagesTest() {
        GoogleHomePageBO homePageBO = new GoogleHomePageBO();
        homePageBO.findByText("Apple");
        Assert.assertNotNull(driver.getTitle());
        homePageBO.clickImageButton();
        Assert.assertTrue(driver.getTitle().contains("Apple"));
        pause(5000);
    }

    @AfterMethod
    void tearDown() {
        LOG.info("After each running.");
        DriverClient.quitDriver();
    }

    @AfterClass
    static void tearDownAll() {
        LOG.info("AfterAll test running.");
        LOG = null;
        driver = null;
    }

    private void pause(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
