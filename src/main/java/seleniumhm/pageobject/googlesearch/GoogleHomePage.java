package seleniumhm.pageobject.googlesearch;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;
import seleniumhm.element.casual.Button;
import seleniumhm.element.casual.TextInput;
import seleniumhm.pageobject.AbstractPage;
import seleniumhm.utils.CustomWaiters;

public class GoogleHomePage extends AbstractPage {
    private static Logger LOG = LogManager.getLogger(GoogleHomePage.class);

    public GoogleHomePage() {
    }

    @FindBy(name = "q")
    private TextInput searchField;

    @FindBy(xpath = "(//a[@class='q qs'])[1]")
    private Button imageButton;

    public GoogleHomePage inputTextInSearchField(String text) {
        CustomWaiters.waitForVisibilityOfElement(searchField).sendKeys(text);
        LOG.info("text :" + text + " was entered");
        return this;
    }

    public GoogleHomePage submitSearchField() {
        CustomWaiters.waitForVisibilityOfElement(searchField).submit();
        LOG.info("search field was submitted");
        return this;
    }

    public GoogleHomePage clickImageButton() {
        CustomWaiters.waitForVisibilityOfElement(imageButton).click();
        LOG.info("image button clicked");
        return this;
    }
}
