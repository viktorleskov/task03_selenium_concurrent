package seleniumhm.element.casual;

import org.openqa.selenium.WebElement;
import seleniumhm.element.PageElement;

public class TextInput extends PageElement {
    public TextInput(WebElement webElement) {
        super(webElement);
    }

    @Override
    public void sendKeys(CharSequence... charSequences) {
        webElement.click();
        webElement.clear();
        webElement.sendKeys(charSequences);
    }

    @Override
    public void clear() {
        webElement.clear();
    }
}
