package cucumber.definitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;
import seleniumhm.pageobject.gmail.businessobject.GmailHomePageBO;
import seleniumhm.pageobject.gmail.businessobject.GmailLoginPageBO;
import seleniumhm.utils.CustomWaiters;

public class StepDefinition {
    private GmailLoginPageBO gmailLoginPageBO = new GmailLoginPageBO();
    private GmailHomePageBO gmailHomePageBO = new GmailHomePageBO();

    @Given("i login with email{string}, password{string} successfully")
    public void loginIntoGmailAccountUsingAppropriateCredentials(String login, String password) {
        gmailLoginPageBO.login(login, password);
    }

    @Then("wait for page load")
    public void waitForPageLoad() {
        gmailHomePageBO.waitForUrlContainsText("inbox");
    }

    @When("write new message")
    public void writeNewMessageInGmail() {
        gmailHomePageBO.prepareMessage("viktorius490@gmail.com", "subject", "message");
    }

    @And("click drafts button")
    public void clickDraftsButton() {
        gmailHomePageBO.clickDraftsButton();
    }

    @And("click at first draft message")
    public void clickFirstDraftMessage() {
        gmailHomePageBO.clickFirstDraftMessage();
    }

    @And("click send message button")
    public void clickSendButton() {
        gmailHomePageBO.clickSendMessageButton();
    }

    @And("click 'view message' button at popup")
    public void clickViewMessageButtonAtPopup() {
        gmailHomePageBO.clickPopUpViewMessageButton();
    }

    @Then("sent messages should opens")
    public void checkSentPageSuccessfullyOpened() {
        Assert.assertTrue(CustomWaiters.waitForUrlContainsText("sent"), "message was not sent");
    }
}
