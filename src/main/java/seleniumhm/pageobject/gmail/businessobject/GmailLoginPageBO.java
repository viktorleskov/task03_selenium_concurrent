package seleniumhm.pageobject.gmail.businessobject;

import seleniumhm.pageobject.gmail.GmailLoginPage;

public class GmailLoginPageBO {
    private GmailLoginPage gmailLoginPage;

    public GmailLoginPageBO() {
        gmailLoginPage = new GmailLoginPage();
    }

    public GmailLoginPageBO login(String email, String password){
        gmailLoginPage
                .inputTextInEmailField(email)
                .clickEmailNextButton()
                .inputTextInPasswordField(password)
                .clickPasswordNextButton();
        return this;
    }

}
